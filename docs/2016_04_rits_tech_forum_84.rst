===================
RITS Tech Forum #84
===================

:日付: 2016/04/04 (月)
:場所: リコーITソリューションズ X42F Blue Ocian (勝どき)
:Web: http://rits-techforum.connpass.com/event/29401/

Sessions
========

★: 聴講 (メモあり)

* Being successful with GitHub (Or Breaking your silos) ★
* How we ship GitHub ★

Memo
====

Being successful with GitHub (Or Breaking your silos)
-----------------------------------------------------

:Speaker: 池田尚史　（GitHub Japan) @ikeike443
:Slide: https://speakerdeck.com/ikeike443/being-successful-with-github-or-breaking-your-silos

導入

* いろいろなところに GitHub flow をおすすめしている
* Branch と merge のコストが低い→プルリクを積極活用
* プルリクで議論とレビューができる
* プルリクを CI できる
* デプロイ記録を残せる
* プルリク単位でリバートできる
* 情報を集約できる
* ビジュアルな blame で変更を分かりやすく追える
* 全文検索にも力を入れている
* Atom, Hubot, GitHub Desktop も使ってね
* 管理ではなくコラボレーション、コラボレーションが人の潜在能力を引き出す！
* サイロを壊す！

How we ship GitHub
------------------

:Speaker: 池田尚史　（GitHub Japan) @ikeike443

Step 1: 何をすべきかを決める

  | Why everything should have a URL.
  | http://ben.balter.com/2015/11/12/why-urls/

* GitHub では全ての会話、ドキュメントは必ず URL を持つように
* メールは URL がないからダメ

Step 2: コードを書く

* 非同期にやる
* 小さな変更でもプルリクを作る。作業中でも WIP とか付けて共有する
* チャット (Slack) を活用、プルリクや CI を追える

デプロイもチャットから

* 透明性を高める、誰がやっても同じプロセス
* master をプルリクブランチにマージ、テスト、デプロイ、haystack (問題があれば master をデプロイ)、master にマージ

Continuous Deployment

* 変更を小さくする。把握しやすい。衝突もしにくい
* 開発中かどうか分かるようにする
* フィーチャーフラグ

1日100回のデプロイを支えるツール

* Chat: シェアードコンソールの役割
* Hubot
* Haystack
* Hackable toolchain

Webhook でイベント通信

* Atmos/Heaven: GitHub style deployment to heroku
* GitHub/jenky: Hubot - Jenkins
