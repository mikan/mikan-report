.. Mikan Report documentation master file, created by
   sphinx-quickstart on Sun May 22 01:48:07 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mikan Report
============

私が参加した IT 勉強会やイベント、セミナー等のレポート集です。

Contents:

.. toctree::
   :maxdepth: 2

   2016_05_java_day_tokyo_2016
   2016_05_jjug_ccc_2016_spring
   2016_04_jjug_night
   2016_04_gocon_2016_spring
   2016_04_rits_tech_forum_84

書いている人
------------

Yutaka Kato (加藤 ゆたか)

* GitHub: `mikan <https://github.com/mikan>`_
* Twitter: `@kagaorange <https://twitter.com/kagaorange>`_

メーカー系企業で働く底辺エンジニアです。。

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
