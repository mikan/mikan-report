=============================
JJUG Night Seminar 2016 April
=============================

:日付: 2016/04/25 (月)
:場所: 日本オラクル 本社19F (外苑前)
:Web: https://jjug.doorkeeper.jp/events/43205
:Hashtag: #jjug

Sessions
========

★: 聴講 (メモあり)

* IDEだとかテキストエディタだとかそんなチャチなもんじゃあ断じてねえ。もっと恐ろしいものの片鱗を味わせてやるぜ ★
* Eclipseでサクサク開発！ そしてクラウドIDEへ ★
* NetBeans IDEの現在とこれから ★

Memo
====

IntelliJ
--------

:Speaker: Yamamoto Yusuke @yusuke / サムライズム


導入

* IntelliJ IDEA (インテリジェイアイディア)
* JetBrains チェコの会社• IDE, collaboration, Language (Kotlin) の 3 領域にプロダクト

Live Templates

  | psvm
  | ↓
  | public static void main(String[] args) { }

Postfix Completion

  | .var
  | ↓
  | TwitterFactory.getSingleton().var → Twitter twitter = TwitterFactory.getSingleton();

  | statuses.for
  | ↓
  | for (Status status : statuses) { }

  | "message".sout
  | ↓
  | System.out.println("message");

ショートカット

* println → printf 変換 (Alt + Enter)

デバッガー

* Chronon debugger: ステップ実行でステップを巻き戻せる！

エディタ機能

* Live edit: HTML 編集をブラウザにリアルタイムに反映、Grunt + browser sync とか不要
* HTML の class 属性からの CSS 定義一発生成
* opt + ↑ → 文脈に即したテキスト選択• 安全な Refactoring
* Commit dialog editing: コミット確認画面から直接ファイル編集できる (デフォルトではロックしてあるのでアンロックボタンを押す)

Terminal

* git
* sl (笑)
* Mac なら bash, Windows なら cmd

Database

* JDBC ドライバ自動ダウンロード• Query 送信 → Table View
* SSH tunnel 機能• Table editing
* 読込専用操作 (check box)
* SQL refactoring

Kotlin

* Better Java みたいなの
* Maven POM から一発 Kotlin プロジェクト化
* Java と共存できる
* Kotlin に Java 貼り付けると変換 (うまくいかないこともある)

Spring Boot

* Inject ジャンプ機能

Java 対応

* Java 8: 登場する 2 年前から対応してたぜ
* Java 9: もう対応してるぜ

感想
^^^^^^

IntelliJ はデモ映えする機能がたくさんあるので楽しいですね。
デモに時間を割いてくれたので、プロの使い手がこんな風に機能を呼び出してるんだってことまで吸収できました。

Lightweight Eclipse ~ up to the Cloud IDE
------------------------------------------

:Speaker: Shin Tanimito @cero_t / Acroquest technology

導入

* Eclipce はシェア No.1 (2014 年のデータだから今は負けたかも)
* なのに、重い、見た目ダサい、プラグイン収集しんどい、などなどの欠点
* でも日本的 SIer (Enterprise) にピッタリだと思うんだよね

Eclipse の良いところ 3 つ

* No.3: Plugins / platform
* No.2: Free
* No.1: Configuration, import, export
* Find bugs, check style とかチーム全員に展開するのが一番ラクな IDE

Eclipse が重くなる 3 つの要因

* No.3: plugins (Pleiades の Ultimate とか使って重いとか言われると悲しい・・・そんなの重いに決まってる)
* No.2: incremental build (in large project)
* No.1: WTP (Web tools platform) for Java EE, validation
* No.0: Anti Virus! (起動が遅い、大量の jar (zip) 内を検査してるせい)
* Java SE Developer を使うのがベスト、または Pleiades platform + JDT

AP Server の人気

* No.1: Tomcat
* No.2: GlassFish (バイアス入ってる？笑)
* Embedded (jetty, boot, etc...)

WTP を使わないでやるには

* Use tomcat plugin
* Run with spring boot
* Maven + remote debug!
* JavaScript 書きたい？Eclipse やめよう（笑） Visual Studio Code がおすすめ。適材適所がいい
* Anti Virus が重い？Eclipse のフォルダを除外してしまえ（自己責任で・・・）
* パフォーマンスの問題は自ら調べて解消してみよう

良いと思うプラグインの紹介

* Check style
* Find bugs
* Quick junit
* DB viewer
* ER master
* (プラグインではないが) STS

そういえば STS のロゴって Clorets にそっくり (笑)

Live Templates

• main → public static void main(String[] args) { }
• sys.out → System.out.println("")

STS

* Spring Boot Dashboard: 複数プロセス同時立ち上げ
* Microservices うまく作れるかが次世代 IDE の鍵、STS は素質ある

今後

* 6月 Neon リリース予定
* 2017 年 Oxygen
* (なんか急に星の名前やめたよね)

Eclipse Che

* Che は「ちぇ」っと読む
* Browser IDE, docker container で動く

Che の DEMO

* ブラウザで動くといっても Android tablet ではまだ苦しく、途中まで開けるが固まるとのこと
* まだまだ開発途上で実用は難があるが、将来性はすごくある
* Cloud IDE なのでサーバーがルールを展開しやすい
* 開発のモビリティがあがる

感想
^^^^^^

先日の GoCon でも登壇した方がこんどは JJUG で Eclipse の発表です。

確かに素の Eclipse 自体は結構軽いということは意外と知られていない (WTP は確かに重い)。

Eclipse Che はまだ発展途上の様子。
クラウド IDE 自体は私も mbed で使ったことがあって、とても良い印象を抱いています。
Java 界隈でも完成度が高いものが出ればヒット間違いないのではと思います。

NetBeans IDEの現在とこれから
------------------------------

:Speaker: Masai Katakai / ORACLE Globalization engineering consultant

導入

* NetBeans 5.0 以来の国際化担当者
* NetBeans はもともとチェコのプラハの学生のプロジェクト
* sun+oracle 統合で心配されたが、うまく棲みわけている
* JDeveloper は enterprise 向け、NetBeans は技術一般向け
* NetBeans が目指す最優先ゴール: 最新の Java テクノロジーをいち早くサポートすること
* JDK bundle release
* HTML5, CSS3
* JS framework,, browser integration, Cordova. node.js

Oracle JET
* Oracle JavaScript Extension Toolkit
* Enterprise 向け JavaScript framework
* jQuery + KnockoutJS + RequireJS
* Alta UI, Alta UI gallery
* Cookbook: その場で試せる◦ Data collection component
* Table

NetBeans 8.2

* ECMAScript 6 対応
* Docker 対応
* 8月リリース予定
* Java Day Tokyo 2016 では 2B, 5B が JET のセッション
* Geertjan (ヒューチャン) が日本に来る！

感想
^^^^^

ほとんど Oracle JET の話でした。
JS はフレームワークの流行り廃りが激しいけれど、Oracle がプッシュすれば Long Term な Support が期待できそうですね（とはいえ KoJS 依存度高そうなので結局同じかも・・・）

個人的には NetBeans は Gradle サポートをもっと強化して欲しい・・・。

おまけ (懇親会)
-----------------

* @yusuke さん：いつでも声かけてくれれば今日やった IntelliJ のデモおたくで披露するよ、とのこと
* @boochnich さん：川崎の読書会に誘われた
* いろんな人から絡んでもらって楽しかったです（小並感）
