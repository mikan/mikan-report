===================
Java Day Tokyo 2016
===================

:日付: 2016/05/24 (火)
:場所: 東京マリオットホテル (品川)
:Web: http://www.oracle.co.jp/events/javaday/2016/
:Hashtag: #JavaDayTokyo

Sessions
========

:詳細: http://www.oracle.co.jp/events/javaday/2016/#secDay1

★: 聴講 (メモあり)

* :ref:`ky` ★
* :ref:`1a` ★
* [1-B] Introduction to MVC 1.0 (JSR 371)
* [1-C] ゴールドマン・サックスのオープンソースへの取り組み
* [1-D] コンテナクラウドとJava Flight Recorderで始めるアジャイル開発
* [1-E] Versatile Java チューニング - 多様な環境での性能考察
* [1-F] Java FX 8のポーティング
* :ref:`2a` ★
* [2-B] NetBeans IDE最新情報
* [2-C] Java EE 7アプリケーションとWebセキュリティ
* [2-D] 使ってみよう！ Oracle Developer Cloud Serviceによるクラウド型開発でJavaEE 7
* [2-E] JavaEEフロントエンド技術適用の実際 ～セキュアな業務APの超高速開発
* [2-F] IoTデバイスとセキュリティ
* [3-A] Putting Hypermedia Back in REST with JAX-RS
* [3-B] JavaFX 8 and the future
* :ref:`3c` ★
* [3-D] 移行事例に基づくJava EEマイグレーションに向けてのアプローチ
* [3-E] Javaの進化にともなう運用性の向上はシステム設計にどういう変化をもたらすのか
* :ref:`4a` ★
* [4-B] What HTTP/2 means to Java Developers?
* [4-C] CDI2.0アップデート＆クックブック
* [4-D] IoTに向けた通信・センサソリューションの開発
* [4-E] 実践して分かったJavaマイクロサービス開発
* :ref:`5a` ★
* [5-B] Introduction to Oracle JET
* [5-C] Javaエンジニアのための“クラウド時代の過ごし方”
* [5-D] オラクルコンサルが語るJava SE 8新機能の勘所
* [5-E] GlobalPlatformの最新動向～Wearable/IoT時代に向けて
* [NS] パネル・ディスカッション - Java Day Night Session with NightHacking Tour

Memo
====

同時通訳ナシにチャレンジしていたので英語セッションのメモはだいぶ怪しいです。。

.. _ky:

[KY] Innovate, Collaborate, with Java
-------------------------------------

:Speaker: 杉原博茂 (日本オラクル, CEO)

* POCO 推し
* 日本は人口減少社会
* IT による生産性向上が急務
* Java は Digital AID, Cloud AID になる
* どう貢献していくか
* TIOBE Index #1, しかも伸びてる
* 身近な Java

:Speaker: Sharat Chander @Sharat_Chander

* 20周年
* Java は成長し続けてる
* Java Magazine 読んでね、無料だよ
* OpenJDK アクティブコミッター増加中
* 1/3 はオラクル社外
* JUG, Java Champions, JCP あげあげで行こう

:Speaker: Heather Vancura @heathervc

* JCP 15周年
* 企業、団体、JUG、独立開発者みんな集まってる
* We need you

:Speaker: Bernard Traversat @BTraTra

* Java 8: Lambda, Streams, Java Mission Control, Flight Recorder, Adv. Mgmt. Console
* 次の20年に向けて
* Security: 再優先
* Density: メモリ削減とか
* 起動時間短縮
* Predictability & 低レイテンシー: More predictable GC pauses
* Profiling, serbicability
* JDK9: モジュール化, 67 の JEP (JDK Enhancement Proposals)
* Oracle JDK と同じスケジュール
* 9 の先: Valhalla, Panama
* Valhalla: Value Types, Specialized Generics, Var Handles
* Panama: Foreign Function Interfaces, Data Layout Control, Arrays 2.0

:Speaker: Geertjan Wielenga @GeertjanW

* NetBeans: Oracle JET 対応, 黒テーマなど
* ユーザー多数
* ねこび～ん

:Speaker: David Delabassee @delabassee

* 過去数年、めまぐるしい変化: Microservices, 分散コンピューティング, Docker, Cloud, DevOps
* On-premise → Cloud: Oracle Cloud, Bluemix, OpenShift
* Microservices は自然な evolution, モノリス → マイクロサービス
* Centralized → Distributed, Stateful → Stateless, Monoglot → Polygrot, 単機能のでっかいチーム → 小規模のクロス機能チーム
* Java で Microservices はアツい
* Modular, Easy, Fast を目指す
* Modular: Jigsaw で無駄のないランタイム
* Easy: JAX-RS, JSON-P
* Fast: NIO, WebSocket, 起動時間短縮
* Java EE API が基盤となる

:Speaker: 浦川伸一 (損保ジャパン日本興亜)

* COBOL を Java リライト中
* 10年使う基幹系システムの開発
* Java EE7 の JSF, CDI, JPA を使うことにした, バッチも使うかも
* Annotation フル活用、バッチレス志向
* Java エンジニアあつまれ

:Speaker: Stephen Chin @steveonjava, Sebastian Daschner @DaschnerS

* バイクツアーで日本を駆け巡った

:Speaker: Yusuke Suzuki (JJUG)

* JJUG 4920人
* ナイトセミナーとか CCC とか
* JCP 貢献
* 参加してね

:Speaker: Stephen Chin @steveonjava, Sebastian Daschner @DaschnerS

* Sebastian が Java Champion になった

:Speaker: 實吉智裕 (Atmark Techno)

* Oracle IOT Cloud Service
* ドローンの活用: 農業、インフラ点検、災害対応など
* Secure IoT Gateway を開発
* SAM (Secure Access Module): SIM と同じ技術、セキュリティ情報を格納できる
* クラウドと通信して機器認証、操縦者認証、ICカード技術でデータ保護、OSGi でソフトの更新

:Speaker: 伊藤敬 (日本オラクル)

* Pepper + Oracle Cloud Platform デモ
* PS Solutions と共同制作
* クラウド上に Oracle JET, Node.js, JSF

.. _1a:

[1-A] Java SE 9 Overview
------------------------

:Speaker: Bernard Traversat @BTraTra

導入

* Java の TIOBE Index がスカイロケットな伸び
* AWS と Google ではナンバーワンの言語、Azure でも３番目
* Security, Density, Startup time, Predictability, Developer Productivity に注力

Java 9

* One modular platform for the Cloud
* module hoge { requires fuga, exports piyo } みたいな感じ
* java -listmods　でモジュール一覧とか
* リンク性能の向上
* セキュリティ向上: 境界が明確になるしね
* JDK9 では多くの internal API をカプセル化する
* G1 GC 採用
* JShell で REPL
* バージョン文字列変更: 1.9.0_25 → 9.1.3 みたいな感じ
* Java Mission Control, Java Flight Recorder
* In-depth profiling and analysis
* 常に有効にしといた
* Advanced Management Console

9 の先

* Object Data Layout
* class Point { final int x; final int y; } で Array 作ったらどうなる？
* Point がいっぱいできるよね
* value class Point なら x, y, x, y って感じで 1 つの配列に詰める
* java/native 相互運用性向上

.. _2a:

[2-A] Project Jigsawではじめるモジュール開発
---------------------------------------------

:Speaker: Yuichi Sakuraba @skrb

導入

* ITPro 連載読んでね
* 本もあるよ (7/8 速攻入門)
* -classpath 問題と標準ライブラリでかい問題

-classpath 問題

* Hadoop なんて 130 個もある
* 抜け漏れ, 競合, 内部変更の影響, 依存性なし、バージョンなし
* public が public すぎる

History

* 2005年、friend 入れたい
* (A) JSR-227: JAM
* (B) JSR-294: superpackager
* 2009年、Mark Reinhold が Project Jigsaw を立ち上げる
* 2017年、Java SE 9!
* OSGi どうすんのとか

つくりかた

* dependency, export, version
* JAR としてビルドされる
* version 機能は今のところかなり poor
* module-info.java をトップレベル (src 直下) において書く
* dependency は requires xxx と書く
* export は exports xxx と書く
* main のあるパッケージの exports 忘れずに
* re-export は requires public xxx と書く
* interface と実装の分離のやり方は連載見て欲しい
* javac -mp xxx, jar --create --file= mods/...
* NetBeans では今のところ開発版を使う
* modulepath, classpath 併用可能、というか普通の jar は unnnamed module 扱い
* jar -p --file mods/xxx.jar でモジュール表示
* java.base は自動的に入る
* java -mp mods -addmods ...
* jar で mainclass 指定していれば、 -m module だけ

標準ライブラリ分割

* java.se@9.0 今までとおなじのに相当
* java.compact1～3 コンパクト版
* dependency check
* jdeps コマンド (実は 8 からある)
* jdeps -s jigsaw.jar (module 見る)
* jdeps jigsaw.jar (中まで見る)
* jlink カスタムランタイム作れる

.. _3c:

[3-C] Java Concurrency, A(nother) Peek Under the Hood
------------------------------------------------------

:Speaker: デイビッド・バック (日本オラクル)

導入

* マルチスレッド関係ない？そんなの学校の宿題だけだよ
* サーバー、GUI、ライブラリ作るとき、バッチ処理も全部マルチスレッドを意識しなくちゃ
* 競合状態、ハイゼンベルグ (バグ)、量子力学、観察者効果

メモリモデル

* メモリモデル: 書き込まれた値が正しく読み込まれる条件の仕様
* シングルスレッドの条件は変わらない
* メモリバリア
* 脱線: 特殊相対性理論、基準系で順序は変わる
* メモリモデルは開発者が頼っていい振舞いを明確にする
* プラットフォームがやっていい最適化を制限する
* Java Memory Model は Java Language Specification の一部
* happened-before
* Leslie Lamport
* シングルスレッド: プログラムオーダー
* syncrnozied
* volatile: Happened-before 成立しない (～1.4)
* final の値が変わる (～1.4)
* Dong Lea: Concurrent Programming in Java の著者
* Java 5 の java.util.concurrent の作者
* JSR-133
* volatile で happened-before 成立化 (5～)
* final がイミュータブル化 (5～)
* volatile は atomic とは違う、Concurent API 使おう
* 高いレベルの API を使うべき
* j.u.concurrent - syncrnozied / wait / notify - volatile / final
* JDK7: fork-join framework
* JDK8: Functional

デモ

* HSDIS (HotSpot 逆アセンブラ)
* GNU Binutils ベース
* base-hsdis
* +printassembly
* スレッドで volatile なしフィールド参照
* client compiler では動く、server compiler では動かない (正しい動作)
* JEP-188: JMM9

.. _4a:

[4-A] Java 9で進化する診断ツール
-----------------------------------

:Speaker: 末永恭正 (NTTコムウェア)
:Slide: http://www.slideshare.net/YaSuenag/java-9-62345544

* jcmd と jhdsb の話
* jcmd コマンド多数追加: VM ドメインの拡充、Compiler ドメインの登場
* JEP-158: Unified JVM Logging
* VM.info で hs_err 並みの情報がいつでも手に入る
* JEP-165: Compiler Control
* ディレクティブの追加やプリント
* JVMTI.agent_load で、動いてるアプリにアタッチするのを jcmd だけでできる
* jcmd はプロセスがハングしたらアタッチできない → jhsdb
* 最後はやっぱり gdb!

.. _5a:

[5-A] 実システムのためのJava EE 7
------------------------------------

:Speaker: Hirofumi Iwasaki (Rakuten) @HirofumiIwasaki

* Rakuten で金融系基幹システム開発
* Java EE7 非常に出来が良い
* 企業システムは寿命は長い
* 先送りすればするほど改修コストが上がる
* 標準 API なら、バージョン間の互換性が高い、一度出たものはほとんど守られている
* Entity Bean は死んだけれど・・・
* EJB がオプションになるかもしれない、CDI に移行しよう
* JSP も、JSF, Facelet に移行しよう
* ”スモールジャンプアップ” を重ねていくのが一番コストが安く長持ちさせられる
* 新しい AP サーバーはバグ多いので、プロダクションでやるには半年ぐらい待ったほうが良い
* 旧仕様からのジャンプ具合 (6 → 7, 5 → 7, 1.4 → 7...) 古いものからジャンプするのは大変
* EJB → CDI は楽
* 旧 JSF → JSF 2.2 移行も楽
