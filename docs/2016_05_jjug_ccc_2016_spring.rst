====================
JJUG CCC 2016 Spring
====================

:日付: 2016/05/21 (土)
:場所: ベルサール新宿グランド (西新宿)
:Web: http://www.java-users.jp/?page_id=2377
:Hashtag: #jjug_ccc

Sessions
========

:詳細: http://www.java-users.jp/?page_id=2396
:スライド集: https://github.com/jjug-ccc/slides-articles-2016spring

★: 聴講 (メモあり)

* [keynote-1] 基調講演1: JJUG運営の戦略と戦術
* :ref:`kn2` ★
* [E-1] テスト自動化のまわりみち
* [F-1] 広告システム刷新の舞台裏ーPHPからJavaに変えてみました
* [GH-1] Type Annotation for Static Program Analysis
* [E-2] ブラウザテストをサクサク進めるためのGeb実践入門
* [F-2] Thymeleaf 3 を使ってみよう！
* [GH-2] Eclipse Collectionsで学ぶコード品質向上の勘所
* [AB-3] Javaでつくる技術ドキュメントのバリデーション環境
* [CD-3] ローカルでの ID 管理は大丈夫 ?! よりセキュアでモダンな Java の多要素認証
* [E-3] Spring Boot で Boot した後に作る Web アプリケーション基盤
* [F-3] Javaエンジニアに知ってほしい、Springの教科書「TERASOLUNA」
* :ref:`gh3` ★
* [I-3] Elasticsearchハンズオン
* [M-3] Spring Bootでチャットツールを作りながらWebの仕組みを理解しよう！
* :ref:`ab4` ★
* [CD-4] ネクストStruts/Seasar2としてのJava EEアクションベースMVC入門
* [E-4] テストゼロからイチへ進むための戦略と戦術
* [F-4] セットアップをスマートにすればJava開発はもっと速くなる！
* [GH-4] 春だからJavaプログラミング再入門
* [AB-5] Apache Apexとインメモリー最適化による超高速処理の世界
* [CD-5] Java EE Web Profileを活用しよう！
* [E-5] Docker on A.* 〜開発から運用まで〜
* [F-5] Javaプログラマーももう逃げられない。マイクロサービスとAPIの世界。
* [GH-5] Spring Framework/Bootの最新情報とPivotalが進めるクラウドネイティブなアプローチ
* :ref:`i5` ★
* [M-5] Python + GDB = Javaデバッガ
* [AB-6] ビッグデータじゃなくても使えるSpark☆Streaming
* [CD-6] SmartNews のニュース配信を支えるサーバ技術
* [E-6] 大規模映像配信サービスのJava8による全面リニューアルの裏側
* [F-6] 人工知能型ERP「HUE」の開発裏話。本当に起こった速度問題とチューニング事例を一挙公開
* [GH-6] Seasar2で作った俺たちのサービスの今
* [I-6_1] Beatsシリーズでお手軽メトリック収集可視化
* :ref:`m61` ★
* :ref:`i62` ★
* [M-6_2] もう知らないとは言わせない！Play Frameworkはじめの一歩
* [AB-7] 若者にJavaの好きなところ嫌いなところきいてみた
* [CD-7] マイクロフレームワーク enkan(とkotowari)ではじめるREPL駆動開発
* :ref:`gh7` ★
* [I-7_1] 新人エンジニアが北海道からでてきてコミュニティについて想うこと
* [I-7_2] SpringBoot+Kotlin 劇的ビフォーアフター
* [M-7_1] 古のJavaを使うということ
* [M-7_2] JVM上でのストリーム処理エンジンの変遷

Memo
====

.. _kn2:

[keynote-2] 基調講演2: Raspberry Pi with Java
------------------------------------------------

:Speaker: Stephan Chin (Oracle Corporation)

導入

* バイク大好き
* JUG 314個、JCP JUG 50以上、コミュニティの広がり

Raspberry Pi で NES エミュレーターを作る

* NES (ファミコン)、CPU は M68k、これを Raspberry Pi でエミュレート
* Pi のボードに載っている二つのポート、何に使う？1つはカメラ、もう1つはスクリーン
* 出力方法の比較: Composite, HDMI, spi, device tree
* Device tree は 画質, 速度, 省電力と3拍子揃っている

ディスプレイ

* 画面のボードに Adafruit Kippah を使う
* これ使うと残る GPIO pin は 6 つ、ファミコンのコントローラーはボタン 8 つ・・・pin 足りない
* Left, right にダイオードを挟み、スイッチにつなぐ、start と一緒にする

Java チューニング

* Java halfnes emulator を使う
* PC 向けに設計、ARM CPU では遅すぎる、チューニングが必要
* Swing Video を Java FX にし、3D アクセラレーション活用
* Array access via unsafe ・・・ doesn't actually help
* Replace loops with System.arraycopy
* PWM audio

3D プリント

* 3D プリンターでケースを制作
* Ultimaker 製プリンター
* ヒンジのプラスチック化

感想
^^^^^^

Raspberry Pi と Java と 3D プリンターで自作のファミコン互換コンソールを作ったお話でした。
会場では実際にコンソールを回し、私もスーパーマリオブラザーズをプレイしました。ちょっともっさりしているような気がしたけれど、気のせいかもしれない。

ソフトもハードも徹底的にこだわる情熱に脱帽でした。

.. _gh3:

[GH-3] Jenkins 2.0
-------------------

:Speaker: Kohsuke Kawaguchi (CloudBees CTO)
:Slide: http://www.slideshare.net/kohsuke/jenkins-20

導入

* 初期は Sun のワークステーションで動かしていた
* 余ったマシンをかきあつめて予算ゼロで分散ビルド環境
* Jenkins の開発はバザールみたいなかんじ、雑然とした感じ、すごいエネルギー
* 当初は個人の趣味だったが、いまは CloudBees を立ち上げて 150 人ぐらいの体制

#1 自動化の波

* ユニットテスト、QAテストスイート実行、パフォーマンステスト、小さな自動化のポケット
* いろんなものを自動化していく
* デプロイ、リリース、ドキュメント生成、様々なテスト
* いろんな職種の人の自動化が広がってきた
* オーケストレーションの必要性
* デプロイ、運用のタスクの自動化まで広がってきた
* CI, CD をつなげる、パイプライン

#2 コード化、GUIやステートを排除

* 変化を見える化、結果ではなく意図を記録、など
* Pipeline Script: スクリプトはソース管理できる (Pipeline as Code)
* Jenkinsfile: フリースタイルプロジェクトでできること + α、並列ビルドの容易な記述、Windows, Linux の書き分け (Groovy DSL)、あらゆるビルド、デプロイ、自動化
* 人間の作業を含める: Input、メッセージ表示、パラメーター入力、自動車工場みたいな
* 実行結果の表示: Stage View
* GitHub Organization Folder: Jenkinsに教えて、あとはJenkinsfile をコミットするだけ、プルリクエストも対象、テストの結果をプルリクエストに反映
* Pipeline Steps Reference
* Docker Pipeline Plugin
* ツールコンテナ、sshagent, timeout,
* 抽象化と再利用: container_build, container_build.groovy
* スケールするパイプライン管理
* 複雑な処理が必要なときにうってつけ

#3 UIの改善

* 新しいジョブ、設定項目のタブ、ステートアップウィザードの改善

#4 組み立てからの脱却

* プラグインで使い方を変えていく、準備が必要
* おすすめプラグインが最初からついてくるようにした
* モジュラーシステムだけど、それなりに使える、Eclipse や IntelliJ もやっていること
* プラグイン同士がオーバーラップする部分の面倒をみる
* 例えば GitHub の設定を入れる箇所の共通化
* 開発、QA資源の集中
* ドキュメンテーション、安心して使えるために

#5 ユーザーを守る

* Jenkins が狙われている
* いままでやってきたこと: セキュリティチームの発足、リリースチームとの連携、セキュリティ勧告と事前アナウンスメント、アプリ内の設定の誘導
* 安全なデフォルトにシフト
* 後方互換性、コアは同じ、今までと同じアップデート方式

Q&A

* パイプラインが失敗したとき、途中からやり直したい。記録も残したい。

  * オープンソースの Jenkins にはない。CloudBees ではチェックポイント機能がある。

* 参考にしたものは。

  * プラグイン。設定まとめるとか。

* マトリクスプラグインが外れている。パイプライン化で発展的解消。

  * どうするかドキュメンテーション作りたい。

* LTS にのるのはいつからか。

  * 6月に検討、7月に出る。それがおそらく 2.x 系。

感想
^^^^^^

川口さん自ら Jenkins の新機能や方向性を解説するというもので、とても貴重なセッションでした。
Pipeline as Code は絶対使っていきたいところ。GitHub や Docker との連携強化も見逃せないです。

発表後に CloudBees Jenkins Platform のチラシを受け取ったので、PDF にして社内に配りました（笑

.. _ab4:

[AB-4] Introduction to JShell: The Java REPL Tool
-------------------------------------------------

:Speaker: Shinya Yoshida (立命館大 M1)
:Slide: http://www.slideshare.net/bitter_fox/introduction-to-jshell-the-java-repl-tool

特徴

* セミコロン不要
* class 書ける
* final 無視
* 一時変数は $n でよびだせる
* 前方参照: 定義する前にクラスを書いて参照できる、Scala はできない
* 電卓にもなる

Java 9 の仕様や機能も確認できる

* アンダーバーの変数ダメ
* 匿名クラスのダイアモンド演算子
* private interface method

コマンド

* / で始める
* /vars /methods /classes /imports
* /list: プログラマーが入力したコマンド

カスタマイズ

* startup: 初期インポート
* feedback: bashrc みたいなもの

その他

* /! 一つ前再実行
* 補完: Tab コード補完、Alt + Enter import、Shift + Tab メソッドシグネチャ
* JShell API: SnippetEvent など
* /debug: ログの情報が見られる
* Project Kulla

JavaFX できない問題

* jfxshell 作った！

感想
^^^^^^

まだ大学院生なのに CCC 登壇、しかも OpenJDK コミッターというすごい人の発表でした。
間に挟むトークも面白く、心底リスペクトしてしまいました。

内容は Java 9 から含まれる予定の JShell (Java Shell) のお話。
単純な Java のシェルとしての機能を超え、使いやすくなるように Java の仕様を LL っぽく拡大しているところが興味深かったです。

個人的には Java 8 の jjs でも事足りる気がしたけれども、それとの絡みは特にありませんでした。なお、shebang には対応していないとのことです。

.. _i5:

[I-5] JavaデスクトッププログラムをふつーのWindowsプログラムのように配布・実行する方法とPCの動きが重くならないよう気を付けること
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

:Speaker: 高橋徹 @boochnich
:Slide: http://www.slideshare.net/torutk/jjug-ccc-2016-spring-i-5-java

前座

* Java読書会BOF: 切れ目をなくし必ず開催するのが続ける秘訣、2人でもやる

アジェンダ

* 1. Java プログラムを使ってもらう
* 2. Windows インストーラー
* 3. メモリと CPU
* 4. 簡単につくる
* 5. あれこれ注文をつける

javapackager

* javapackager: jdk に入っている, 再配布用パッケージ作成機能
* WiX Toolset 3.10 MS 製 OSS
* ネイティブ形式 (MSI 形式) で吐ける
* Program Files にアプリの jar とランタイムが置かれる

チューニング

* Hyper threading: 64bit JVM と 32bit JVM で初期 GC スレッド数やヒープサイズが大きく異なる
* Process Explorer を使って分析

NetBeans + javapackager

* NetBeans IDE で Windows インストーラーを作成
* デプロイメント -> ネイティブパッケージの有効化
* 恒久的に… で Program Files か User か変わる (わかりづらい)

いくつか欠点

* 前のバージョンが入ったところに次のバージョンをいれても別々のアプリになる
* インストーラー UI なし
* WIX Toolset: candle.exe + night.exe
* javapackager -v で中間ファイル
* template.wxs
* UpgradeCode MajourUpgrade を編集

Q&A

* よくあるやつはピロセス名が Java になっちゃう

  * packager なら大丈夫！

* JVM オプションどのぐらい柔軟に設定できるか

  * コマンドオプション、インストール後の .cfg 編集

* アップデートを強制は可能か

  * No

* データファイルを追加する場合は

  * コマンドオプションでファイル指定、フォルダは無理、WiX で指定や Install Sheild

* 32bit JRE の欠点は

  * メモリ上限の違い
  * Hotspot VM の server/client

感想
^^^^^^

私も先月からお世話になっている Java 読書会 BOF の代表の高橋さんのセッションで、冒頭にその紹介があり、場を温めていました。

javapackager は JavaFX アプリの配布で使ったことがあったのですが、ネイティブインストーラー作成機能があったなんて・・・、目からうろこでした。

社内でもフィールドエンジニア向けツールを Java で作ってバッチファイルかブートストラップ用 exe かを同梱して配布したりしている場面をみたことがあります。
ぜひこのスライドを参考にしてより良いツールに仕立てて欲しいですね。

.. _m61:

[M-6_1] 十徳ナイフとしてのGradle
----------------------------------

:Speaker: @grimrose
:Slide: https://nbviewer.jupyter.org/format/slides/github/grimrose/JJUG-CCC-2016-Spring/blob/master/Gradle%20as%20Army%20Knife.ipynb#/
:Samples: https://github.com/grimrose/JJUG-CCC-2016-Spring

内容

* みんな Gradle 徹底入門読もうな
* サンプルおいておくんでコピペして使ってね

(メモがほとんど取れていない)

感想
^^^^^^

比較的小さな部屋だったのですが大勢詰めかけたので立ち見＆直座りが出ました。ちなみに私もスクリーン真ん前に直座り。

「Gradle 徹底入門」はちょうど私の読書会 (`AOSN 読書会 <http://aosn.github.io/>`_) で読み終わったばかりだったので、この発表にも高い関心を持っていました。

タイムリーな Kotlin ネタが少し入っていましたが、これはまだ実用的ではなさそうです。
すごい勢いでスライドが流れていったので、のちほど読み返す予定です。

.. _i62:

[I-6_2] OpenJDK コミュニティに参加してみよう
----------------------------------------------

:Speaker: Yuji kubota @sugarlife
:Slide: http://www.slideshare.net/YujiKubota/openjdk-jjug

導入

* 困ったらどこを読む？
* OpenJDK Wiki

要望提案

* 説明するより ML にパッチをだすのが速い
* または JEP として機能提案

参加方法

* OCA にサインして送る
* 基本は最新のJDK、その後バックポート
* OpenJDK のコードは mercurial
* 「プロジェクト名/リポジトリ名/サブリポジトリ名」という構造
* ML 151個もある…
* jdkX (jdk9) は全体用、議論の場ではない
* パッチはメール本文に貼る、添付や外部 URL はみてくれない

役割

* Author, Committer, Reciewer
* 投票で決まる

感想
^^^^^^

さすが OpenJDK コミッターという内容の発表でした。

コミュニティ活動したいですね。

.. _gh7:

[GH-7] Java Puzzlers
--------------------

:Speaker: Yoshio Terada (MSKK) / Yuichi Sakuraba @skrb
:Slide: http://www.slideshare.net/tyoshio2002/java-puzzlers-jjug-ccc-2016

Q1

* x (正解: 期待結果が変わる)
* MIN_VALUE / Math.abs(MIN_VALUE) = +1 (-1 ではない)

Q2

* o (正解: 素直な結果)
* int + long はエラー、int += long は OK、IntStream.rangeClosed(1,10).sum() もいい

Q3

* o (正解: 素直な結果)
* .forEach(builder::append)、effectively final、メソッド参照

Q4

* o (正解: コンパイルエラー)
* Integer List<Integer> filter((int i) ->... 期待した型 (int) に合わない (Integer)

Q5

* o (正解: 期待結果が変わる)
* HashMap のハッシュコードはキャッシュされる
* Mutable value を key にしてはいけない

感想
^^^^^^

おなじみらしいけれど、私自信は Pazzler セッション初参加（参戦？）です。
5問出題され、全問正解2人、4問正解3人という難易度でした。

そしてなんと、私も4問正解してしまったので、桜庭先生から著書「`Java SE 7/8 速攻入門 <http://www.amazon.co.jp/gp/product/4774177385/ref=as_li_ss_il?ie=UTF8&camp=247&creative=7399&creativeASIN=4774177385&linkCode=as2&tag=3320-22>`_」を頂いてしまいました！
とてもうれしいです。ありがとうございます！(あ、桜庭先生のサイン頂き損ねました。。)

ちなみに最後の問題は、柴田芳樹先生の指導を受けた私としては絶対に間違えてはいけない問題でした。

  | ハッシュテーブルの季節：柴田 芳樹 (Yoshiki Shibata)：So-netブログ
  | http://yshibata.blog.so-net.ne.jp/2012-05-13


懇親会 & LT大会
--------------------

大爆笑の連続で面白かったです（小並感）

# メモ取っていません。。

午前のセッションから懇親会まで参加したので、ヘトヘトになりました・・・。

24 日は Java Day Tokyo 2016 もあるので、イベント目白押しですね。JDT のほうも後ほどこの場でレポートをお送りします。
