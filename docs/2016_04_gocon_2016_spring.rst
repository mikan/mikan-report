=========================
Go Conference 2016 Spring
=========================

:日付: 2016/04/23 (土)
:場所: サイバーエージェント セミナールームB (渋谷)
:Web: http://gocon.connpass.com/event/27521/
:Hashtag: #gocon

Sessions & Memo
===============

Keynote
-------

:Speaker: Dave Cheney @davecheney
:Slide: http://dave.cheney.net/paste/gocon-spring-2016.pdf

Sentinel errors や error types をやめようとかいう話と、GitHub - pkg/errors: Functions for manipulating errors の紹介。

* errors.Wrap
* errors.Print
* errors.Cause

使ってみたいですね。

Auto-Generating builderscon server, validator, and client with JSON (Hyper)? Schema, et al
------------------------------------------------------------------------------------------

:Speaker: Daisuke Maki @lestrrat / HDE
:Slide: https://docs.google.com/presentation/d/1D56wtLyoZGZddkoKlmJQ1SXgmqJFNXRHmFOPZ7vuy8Q

JSON Hyper Schema から Go コードをゴリゴリ自動生成するロックなお話でした。

Reading Go tools
----------------

:Speaker: 芝村博石 @motemen / Hatena
:Slide: https://speakerdeck.com/motemen/reading-go-tools-gocon-2016-spring

goimports と gddo (go doc dot org) の構造など。

GoでGUI
-------

:Speaker: Yoshiki Shibukawa @Shibukawa / DeNA
:Slide: http://www.slideshare.net/shibukawa/go-multi-platform-gui-trials-and-errors

Qt バインディングの改善に挑戦している話でした。

確かに、Go の GUI はまだまだ不毛の地・・・。私もちょこっとかじって大変もがきました。

Elastic Beats を導入した話
------------------------------

:Speaker: Daichi Hirata @daichild / Cyber Agent
:Slide: https://speakerdeck.com/daic_h/go-conference-2016-spring

Apache Kafka とか Kibana とか絡めて。

残念ながら、このあたりは私の守備範囲外です。。

GRPCの実践と現状での利点欠点
--------------------------------

:Speaker: Masahiro Sano @kazegusuri / Mercari
:Slide: https://speakerdeck.com/kazegusuri/go-conference-2016-spring

Stubby から GRPC に移行した話。

Go と GAE による Web サービス開発
---------------------------------

:Speaker: Tatsuya Tsuruoka @ttsuruoka / Mercari Atte
:Slide: https://speakerdeck.com/ttsuruoka/gotogaeniyoruwebapurikesiyonkai-fa-go-con-2016-spring

自作Webフレームワーク uconを作った話
-------------------------------------

:Speaker: @vvakame / Top Gate
:Slide: http://www.slideshare.net/vvakame/gocon2016-spring-web-ucon

https://github.com/favclip/ucon

Google APIs Explorer が便利: https://developers.google.com/apis-explorer/

Go のサンプル集も揃ってる: https://github.com/google/google-api-go-client

How to Contribute to Golang
---------------------------

:Speaker: Shinji Tanaka @stanaka / Hatena (CTO)
:Slide: https://speakerdeck.com/stanaka/how-to-contribute-go

まさかのアセンブリ言語部分のパッチ！

Requirements for Go server in production
----------------------------------------

:Speaker: Yoshiki Nakagawa @yyoshiki41 / eureka
:Slide: http://go-talks.appspot.com/github.com/yyoshiki41/go-sever-requirements/main.slide

Golang with Google Cloud Platform
---------------------------------

:Speaker: Shintaro Kaneko @kaneshin / eureka
:Slide: https://speakerdeck.com/kaneshin/golang-plus-cloud-vision-api-plus-gae-gocon-spring

Building scalable MO game server
--------------------------------

:Slide: Naoki Inada @methane / KLab
:Speaker: https://docs.google.com/presentation/d/1pSywpKera0huKCHDVGPVA2jQMvy-Pk8IR9s7AAVojDM

Goでデーモンを作る (LT)
-----------------------

:Speaker: Hironobu Saitoh @hironobu_s / GMO Internet
:Slide: https://speakerdeck.com/hironobu/godedemonwozuo-tutemiru

Goroutine on Google App Engine (LT)
-----------------------------------

:Speaker: Shingo Ishimura @sinmetal / Top Gate
:Slide: https://sinmetal-slide.appspot.com/2016/gocon0423/gocon0423.slide

Gopherに逆らうとどうなるのか (LT)
---------------------------------

:Speaker: Seiji Takahashi @__timakin__ / DeNA
:Slide: https://speakerdeck.com/timakin/gohpernini-rautodounarufalseka

Goによる格闘ゲーム用マクロ開発記 (LT)
--------------------------------------

:Speaker: Shin Tanimoto @cero_t / Acroquest Technology
:Slide: https://github.com/cero-t/cero-macro.go

マイクロサービスのライブラリを見比べてみた (LT)
-----------------------------------------------

:Speaker: Yuki Matsumoto @y_matsuwitter / Gunosy
:Slide: https://speakerdeck.com/ymatsuwitter/comparison-between-golang-microservices-frameworks


Go言語の入門書を書いてみた話 (LT)
------------------------------------

:Speaker: 松尾愛賀
:Slide: http://www.shoeisha.co.jp/book/detail/9784798142418

GoImagick の詳解 (LT)
-------------------------

:Speaker: よや @yoya
:Slide: https://speakerdeck.com/yoya/goimagicksyokai

Go Report Card (LT)
--------------------

:Speaker: Shawn Smith @shawnps / Signal Sciences
:Slide: https://goreportcard.com/

Go Report Card, 私も愛用しています。シンプルなのにモチベーションが上がる良いツールですね。
